# dotfiles

This repo contains my configurations (and more) from my `$HOME` and can easily
be deployed with a few commands on any POSIX-compliant platform.

## Installation

After setting up a fresh machine I run:

```shell
cd ~
git init
git remote add origin git@gitlab.com:augensalat/dotfiles.git
git fetch
git switch -f master
```

At work (or for any non-private use) I can configure a different email address
in an extra file `.config/git/config.local` like so:

```shell
git config --file ~/.config/git/config.local user.email me@example.com
```
